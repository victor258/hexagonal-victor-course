# Notas

## Docker

MYSQL_ROOT_PASSWORD=root
MYSQL_USER=admin
MYSQL_PASSWORD=root
MYSQL_DATABASE=cursonode
Puerto: 3310:3306

```sh
docker run -d -p 3310:3306 --name mysqlserver -e MYSQL_ROOT_PASSWORD=root -e MYSQL_USER=admin -e MYSQL_PASSWORD=root -e MYSQL_DATABASE=cursonode mysql:8
```

Con volumen nombrado:

```sh
docker run -d -p 3310:3306 -v volu-mysql:/var/lib/mysql --name mysqlserver -e MYSQL_ROOT_PASSWORD=root -e MYSQL_USER=admin -e MYSQL_PASSWORD=root -e MYSQL_DATABASE=cursonode mysql:8

docker volume ls
```

```sh
docker stop name_container
```

```sh
docker start name_container
```

```sh
docker kill name_container
```

```sh
docker logs name_container
```

```sh
docker inspect name_container
```

## Ejecutar Dockerfile

```sh
docker build -t name_image:1.0.0 .
```

```sh
docker run -d -p 3000:3000 --name name_container name_image
```

## Docker Compose

```sh
docker-compose up -d
```

```sh
docker-compose down
```

## Docker network

```sh
docker network create name_network
```

```sh
docker network ls
```
