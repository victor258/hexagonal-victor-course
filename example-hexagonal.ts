class Teacher {
  constructor(
    public name: string,
    public lastname: string,
    public cod: number,
  ) {}
}

interface TeacherRepository {
  insert(teacher: Teacher): Teacher;
  validationNationality(teacher: Teacher): boolean;
}

class TeacherApplication {
  constructor(private infraestructure: TeacherRepository) {}

  insert(teacher: Teacher) {
    const isValid = this.infraestructure.validationNationality(teacher);
    if (!isValid) {
      return null;
    }
    const insertedTeacher = this.infraestructure.insert(teacher);
    return insertedTeacher;
  }
}

class TeacherInfraestructure implements TeacherRepository {
  insert(teacher: Teacher) {
    return teacher;
  }
  validationNationality(teacher: Teacher) {
    console.log(teacher);
    return true;
  }
}

const teacher: Teacher = new Teacher('Miguel', 'Chamorro', 22);
const infraestructure = new TeacherInfraestructure();
const application = new TeacherApplication(infraestructure);
console.log(application.insert(teacher));
