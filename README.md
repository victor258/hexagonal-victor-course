# Proyecto final Node + TS + Hexagonal

## Endpoints (Documentación Postman)

- [Visitar aquí](https://documenter.getpostman.com/view/6804314/2s9XxtwuiW)

## Instalar dependencias

```bash
yarn install
```

## Levantar contenedor mysql

```bash
docker compose up -d
```

## Construir imagen

```bash
docker build -t node-ts-hexagonal:1.0.1 .
```

## Crear Network

```bash
docker network create node-app-net
```

## Correr contenedor

```bash
docker run -d --name node-ts-hexagonal -p 3000:3000 -e DB_HOST=cont-mysqlserver -e DB_PORT=3306 --network node-app-net node-ts-hexagonal:1.0.1
```

## Docker Network Connect

```bash
docker network connect node-app-net cont-mysqlserver
```

## Docker logs

```bash
docker logs node-ts-hexagonal
```
