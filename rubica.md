## DESCRIPCIÓN:

El proyecto final es libre, por tanto, pueden crearlo basado en cualquier idea que estimen conveniente, por ejemplo crear un backend con un modelo de datos de restaurant, clínica, forestal, etc, para la creación de un CRUD. En caso de copia entre compañeros, quién lo haya entregado primero será evaluado y los demás se evaluarán con 0, mismo caso, si envían mal el código se evalúa con 0, ya que es importante revisar las cosas antes de enviarlas y ser profesionales.

## SE EVALUARÁ

1. Aplicar arquitectura limpia Hexagonal a lo largo de todo el flujo del proyecto con su debida organización de directorios. 25%
2. Definir configuraciones globales del proyecto: nodemon, editor config, eslint, prettier, tsconfig, variables de entorno, bdd y servidor. 5%
3. Creación de al menos 1 modelo de bdd, pueden escoger la bdd que quieran, ya sea relacional o no. 5%
4. Creación de rutas padres e hijas mediante http incluyendo la de health check y algún uso de middleware aplicado en alguna de ellas. 10%
5. Manejo de excepciones de error personalizados de la app + validaciones del request mediante class-validators 10%
6. Se deben ocupar principios solid, patrones de diseño y abstracciones de código limpio. Se debe dejar un comentario donde hagan uso de patrones de diseño y principios solid. Deben aplicar al menos 3 principios solid y al menos 4 patrones de diseño 20%.
7. Deben aplicar los principios Owasp de seguridad, habilitación de comunicación entre dominios (CORS) y optimizaciones en los buffer de los json. 5%
8. Deben generar el build de manera optimizada con gulp y probar que la app corre con js. 5%
9. Deben construir un Dockerfile para la app y un Docker Compose para la bdd y validar que su imágenes de bdd y app funcionen correctamente. 15%.

## ENTREGABLES

1. Link del repositorio con la especificaciones de levantamiento desde un README.md.
2. Nombre completo y rut o dni dependiendo del país.
3. Enviar a mi correo: mchamorro@escalab.tech con asunto "Envío proyecto final - Node.js Curso Tarde".

## CONSULTAS

1. Para consultas pueden apoyarse por el grupo del curso, como también me pueden comunicar por interno por wsp o por correo.

## FECHAS DE PROYECTO FINAL

1. Fecha de Inicio: Jueves 08 de junio
2. Fecha de entrega: Jueves 13 de julio hasta las 23:59.
3. Para aprobar deben cumplir un 73% de 100%, equivalente este 73 a un 5.0 de nota mínima.

Saludos y mucho éxito a todos/as, a por esa insignia de certificación y a probar que ustedes pueden!!.
