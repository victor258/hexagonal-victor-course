import { Request, Response } from 'express';
import { IError } from 'src/modules/user/interfaces/helpers/ierror';

class Errors {
  static notFound(_req: Request, res: Response): void {
    res.status(404).json({
      message: 'Not Found',
    });
  }

  static genericError(err: IError, _req: Request, res: Response): void {
    res.status(err.status || 500).json({
      message: err.message,
      stack: err.stack,
    });
  }
}

export default Errors;
