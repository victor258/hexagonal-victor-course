import { Router, Request, Response } from 'express';

class RouterHealth {
  readonly router: Router;

  constructor() {
    this.router = Router();
    this.mountHealthCheck();
  }

  mountHealthCheck(): void {
    this.router.get('/', (req: Request, res: Response) => {
      res.status(200).json({
        message: 'Server is up and running',
      });
    });
  }
}

export default new RouterHealth().router;
