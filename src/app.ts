import express, { Application } from 'express';
import routerHealth from './helpers/health';
import HandlerErrors from './helpers/errors';

import routerUser from './modules/user/interfaces/http/user.routes';
import routerMachine from './modules/machine/interfaces/http/machine.routes';

import hpp from 'hpp';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';

class App {
  readonly app: Application;

  constructor() {
    this.app = express();
    this.owaspSecurityMiddleware();
    this.mountHealthCheck();
    this.mountMiddlewares();
    this.mountRoutes();
    this.mountErrorHandlers();
  }

  owaspSecurityMiddleware(): void {
    this.app.use(hpp());
    this.app.use(helmet());
    this.app.use(
      cors({
        origin: '*',
        optionsSuccessStatus: 200,
        methods: ['GET', 'POST', 'PUT', 'DELETE'],
      }),
    );
  }

  //  principio SOLID: Open Closed Principle
  mountHealthCheck(): void {
    this.app.use('/health', routerHealth);
  }

  mountMiddlewares(): void {
    this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }

  mountRoutes(): void {
    this.app.use('/api/v1/user', routerUser);
    this.app.use('/api/v1/machine', routerMachine);
  }

  mountErrorHandlers(): void {
    this.app.use(HandlerErrors.notFound);
    this.app.use(HandlerErrors.genericError);
  }
}

export default new App().app;
