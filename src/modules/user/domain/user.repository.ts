import User from './user';
import { UserNotFoundException } from './exceptions/user.exception';
import { Result } from 'neverthrow';
import { UserUpdate } from './interfaces/userUpdate.interface';

// Princio SOLID: Dependency Inversion Principle
export interface UserRepository {
  // design pattern Facade
  list(): Promise<User[]>;
  listOne(guid: string): Promise<Result<User, UserNotFoundException>>;
  insert(user: User): Promise<Result<User, UserNotFoundException>>;
  update(
    guid: string,
    user: Partial<UserUpdate>,
  ): Promise<Result<User, UserNotFoundException>>;
  delete(guid: string): Promise<Result<User, UserNotFoundException>>;
  login(user: User): Promise<Result<User, UserNotFoundException>>;
}
