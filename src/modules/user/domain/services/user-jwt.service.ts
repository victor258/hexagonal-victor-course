import jwt from 'jsonwebtoken';

export class UserJWTService {
  static sign(email: string): string {
    const JWT_SECRET = process.env.JWT_SECRET || 'secret';
    return jwt.sign({ email }, JWT_SECRET);
  }
}
