import bcrypt from 'bcryptjs';

export class UserPasswordService {
  static async hash(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
  }
  static async compare(password: string, dbPassword: string): Promise<boolean> {
    return bcrypt.compare(password, dbPassword);
  }
}
