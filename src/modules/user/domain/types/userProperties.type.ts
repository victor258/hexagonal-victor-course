import { EmailVO } from '../value-objects/email.vo';

export interface UserOptional {
  token: string;
  active: boolean;
  guid: string;
}

export interface UserRequired {
  name: string;
  lastname: string;
  email: EmailVO;
  password: string;
}

export type UserProperties = Required<UserRequired> & Partial<UserOptional>;
