export abstract class ValueObject<Props> {
  protected readonly props: Props;

  constructor(props: Props) {
    this.props = Object.freeze(props);
  }
}

// Al utilizar la clase ValueObject como base para objetos de valor, todos los objetos de valor creados a partir de ella
// heredarán este comportamiento de inmutabilidad.
// Cada vez que se crea una instancia de un objeto de valor, sus atributos se congelan y no pueden ser modificados.

// Esto asegura que los objetos de valor sean utilizados de manera consistente en todo el código,
// ya que no es posible modificar sus atributos de forma inadvertida o accidental.
// Además, al ser inmutables, los objetos de valor pueden compartirse de manera segura y
// utilizarse en contextos donde se requiere una mayor confiabilidad y consistencia de los datos.
