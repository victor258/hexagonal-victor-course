import { validate as uuidValidate } from 'uuid';
import { ValueObject } from './vo.class';
import { UserGuidInvalidException } from '../exceptions/user.exception';
import { err, ok, Result } from 'neverthrow';
// import { DomainException } from '../exceptions/domain.exception';

interface GuidProps {
  value: string;
}

type GuidResult = Result<GuidVo, UserGuidInvalidException>;

export class GuidVo extends ValueObject<GuidProps> {
  private constructor(props: GuidProps) {
    super(props);
  }

  static create(guid: string): GuidResult {
    if (!uuidValidate(guid)) {
      return err(new UserGuidInvalidException());
    }

    return ok(new GuidVo({ value: guid }));
  }

  get value(): string {
    return this.props.value;
  }
}
