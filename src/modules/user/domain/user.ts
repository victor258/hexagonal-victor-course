import { IEntity } from '../../shared/entity.interface';
import { EmailVO } from './value-objects/email.vo';
import { UserUpdate } from './interfaces/userUpdate.interface';
import { UserProperties } from './types/userProperties.type';

export default class User implements IEntity<UserProperties, UserUpdate> {
  private readonly guid: string;
  private name: string;
  private lastname: string;
  private readonly email: EmailVO;
  private password: string;
  private token: string;
  private active: boolean;

  constructor(properties: UserProperties) {
    this.active = true;
    Object.assign(this, properties);
  }

  properties(): UserProperties {
    return {
      name: this.name,
      lastname: this.lastname,
      email: this.email,
      password: this.password,
      token: this.token,
      active: this.active,
      guid: this.guid,
    };
  }

  update(fields: UserUpdate) {
    Object.assign(this, fields);
  }

  delete() {
    this.active = false;
  }
}
