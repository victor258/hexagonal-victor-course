import { v4 as uuidv4 } from 'uuid';
import { UserPasswordService } from './services/user-password.service';
import { UserJWTService } from './services/user-jwt.service';

import User from './user';
import { UserProperties } from './types/userProperties.type';
import { EmailVO } from './value-objects/email.vo';

import {
  UserLastnameRequiredException,
  UserNameRequiredException,
  UserPasswordRequiredException,
  UserPasswordLengthInvalidException,
  UserEmailInvalidException,
  UserEmailRequiredException,
} from './exceptions/user.exception';
import { err, ok, Result } from 'neverthrow';

export type UserResult = Result<
  User,
  | UserNameRequiredException
  | UserLastnameRequiredException
  | UserPasswordRequiredException
  | UserPasswordLengthInvalidException
  | UserEmailInvalidException
  | UserEmailRequiredException
>;

interface UserCreateProps {
  name: string;
  lastname: string;
  email: string;
  password: string;
}

export type UserLoginResult = Result<
  User,
  | UserEmailRequiredException
  | UserPasswordRequiredException
  | UserPasswordLengthInvalidException
  | UserEmailInvalidException
>;

// design pattern AbstractFactory
export default class UserFactory {
  login({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): UserLoginResult {
    if (!email || !email.trim()) {
      return err(new UserEmailRequiredException());
    }

    if (!password || !password.trim()) {
      return err(new UserPasswordRequiredException());
    }

    if (password.length < 5) {
      return err(new UserPasswordLengthInvalidException(password));
    }

    const emailResult = EmailVO.create(email);

    if (emailResult.isErr()) {
      return err(new UserEmailInvalidException());
    }

    // Design Pattern: Factory Method
    const token = UserJWTService.sign(email);

    const userProperties: UserProperties = {
      name: null,
      lastname: null,
      email: emailResult.value,
      password,
      guid: null,
      token,
    };

    const user = new User(userProperties);

    return ok(user);
  }

  async create({
    name,
    lastname,
    email,
    password,
  }: UserCreateProps): Promise<UserResult> {
    if (!name || !name.trim()) {
      return err(new UserNameRequiredException());
    }

    if (!lastname || !lastname.trim()) {
      return err(new UserLastnameRequiredException());
    }

    if (!password || !password.trim()) {
      return err(new UserPasswordRequiredException());
    }

    if (password.length < 5) {
      return err(new UserPasswordLengthInvalidException(password));
    }

    if (!email || !email.trim()) {
      return err(new UserEmailRequiredException());
    }

    const emailResult = EmailVO.create(email);

    if (emailResult.isErr()) {
      return err(new UserEmailInvalidException());
    }

    // Design Pattern: Factory Method
    const passwordHash = await UserPasswordService.hash(password);

    const guid = uuidv4();

    // Design Pattern: Factory Method
    const token = UserJWTService.sign(email);

    const userProperties: UserProperties = {
      name,
      lastname,
      email: emailResult.value,
      password: passwordHash,
      guid,
      token,
    };

    const user = new User(userProperties);

    return ok(user);
  }
}
