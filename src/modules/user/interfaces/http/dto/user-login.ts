import { UserProperties } from 'src/modules/user/domain/types/userProperties.type';
import { DTO } from './dto.generic';

interface UserDTO {
  name: string;
  lastname: string;
  email: string;
  guid: string;
  token: string;
}

export type UserLoginDTO = UserDTO;

export class UserLoginMapping extends DTO<UserProperties, UserLoginDTO> {
  execute(data: UserProperties): UserLoginDTO {
    return {
      name: data.name,
      lastname: data.lastname,
      email: data.email.value,
      guid: data.guid,
      token: data.token,
    };
  }
}
