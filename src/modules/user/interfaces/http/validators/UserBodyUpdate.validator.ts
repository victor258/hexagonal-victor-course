import {
  IsNotEmpty,
  IsOptional,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';

export class BodyUpdate {
  @IsOptional()
  @IsString({ message: 'El nombre del user debe ser un string' })
  @IsNotEmpty({ message: 'El nombre del user no puede estar vacío' })
  @MinLength(2, {
    message: 'El nombre del user debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El nombre del user no puede contener solo espacios vacíos',
  })
  name?: string;

  @IsOptional()
  @IsString({ message: 'El apellido del user debe ser un string' })
  @IsNotEmpty({ message: 'El apellido del user no puede estar vacío' })
  @MinLength(2, {
    message: 'El apellido del user debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El apellido del user no puede contener solo espacios vacíos',
  })
  lastname?: string;

  @IsOptional()
  @IsString({ message: 'la constraseña debe ser un string' })
  @IsNotEmpty({ message: 'la constraseña no puede estar vacío' })
  @MinLength(6, {
    message: 'la constraseña debe tener al menos 6 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'la constraseña no puede contener solo espacios vacíos',
  })
  password?: string;
}
