import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';

export class BodyLogin {
  // Design Pattern: Decorator
  @IsString({ message: 'El email debe ser un string' })
  @IsNotEmpty({ message: 'El email no puede estar vacío' })
  @IsEmail({}, { message: 'El email debe ser un email válido' })
  email: string;

  @IsString({ message: 'password must be a string' })
  @IsNotEmpty({ message: 'password must not be empty' })
  @MinLength(6, { message: 'password is too short' })
  @Matches(/^(?!^\s*$).+/, {
    message: 'la constraseña no puede contener solo espacios vacíos',
  })
  password: string;
}
