import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';

export class BodyInsert {
  @IsString({ message: 'El nombre del user debe ser un string' })
  @IsNotEmpty({ message: 'El nombre del user no puede estar vacío' })
  @MinLength(2, {
    message: 'El nombre del user debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El nombre del user no puede contener solo espacios vacíos',
  })
  name: string;

  @IsString({ message: 'El apellido del user debe ser un string' })
  @IsNotEmpty({ message: 'El apellido del user no puede estar vacío' })
  @MinLength(2, {
    message: 'El apellido del user debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El apellido del user no puede contener solo espacios vacíos',
  })
  lastname: string;

  @IsString({ message: 'la constraseña debe ser un string' })
  @IsNotEmpty({ message: 'la constraseña no puede estar vacío' })
  @MinLength(6, {
    message: 'la constraseña debe tener al menos 6 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'la constraseña no puede contener solo espacios vacíos',
  })
  password: string;

  @IsString({ message: 'El email debe ser un string' })
  @IsNotEmpty({ message: 'El email no puede estar vacío' })
  @IsEmail({}, { message: 'El email debe ser un email válido' })
  email: string;
}
