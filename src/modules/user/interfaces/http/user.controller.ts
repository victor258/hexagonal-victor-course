import { Request, Response } from 'express';
import { GuidVo } from '../../domain/value-objects/guid.vo';
import { IError } from '../helpers/ierror';
import UserFactory from '../../domain/user-factory';
import UserApplication from '../../application/user.application';
import { UserInsertMapping } from './dto/user-insert.dto';
import { UserListMapping } from './dto/user-list.dto';
import { UserListOneMapping } from './dto/user-list-one.dto';
import { UserUpdateMapping } from './dto/user-update.dto';
import { UserDeleteMapping } from './dto/user-delete.dto';
import { UserLoginMapping } from './dto/user-login';

export default class {
  // Design Pattern: Dependency Injection
  // Solid Principle: Dependency Inversion
  constructor(private application: UserApplication) {
    // Design Pattern: mediator or controller
    this.insert = this.insert.bind(this);
    this.list = this.list.bind(this);
    this.listOne = this.listOne.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
    this.login = this.login.bind(this);
  }

  async login(req: Request, res: Response) {
    const { email, password } = req.body;

    // 🙌🏽 Validaciones email y password: retorna new JWT
    const userToken = new UserFactory().login({
      email,
      password,
    });

    // ❌ Si no pasa las validaciones, retorna un error
    if (userToken.isErr()) {
      const err: IError = new Error(userToken.error.message);
      return res.status(400).json({ message: err.message });
    }

    try {
      const userResult = await this.application.login(userToken.value);

      if (userResult.isErr()) {
        const err: IError = new Error(userResult.error.message);
        err.status = 404;
        return res.status(404).json({ message: err.message });
      }

      if (userResult.isOk()) {
        const result = new UserLoginMapping().execute(
          userResult.value.properties(),
        );
        return res.json(result);
      }
    } catch (error) {
      return res.status(500).json({ message: error.message });
    }
  }

  async insert(req: Request, res: Response) {
    const { name, lastname, email, password } = req.body;

    // este es el que valida los campos del usuario! 🙌🏽
    const newUser = await new UserFactory().create({
      name,
      lastname,
      email,
      password,
    });

    if (newUser.isErr()) {
      const err: IError = new Error(newUser.error.message);
      return res.status(400).json({ message: err.message });
    }

    try {
      const userResult = await this.application.insert(newUser.value);
      if (userResult.isErr()) {
        const err: IError = new Error(userResult.error.message);
        return res.status(404).json({ message: err.message });
      }

      if (userResult.isOk()) {
        const result = new UserInsertMapping().execute(
          userResult.value.properties(),
        );
        return res.json(result);
      }
    } catch (error) {
      return res.status(500).json({ message: error.message });
    }
  }

  async list(_req: Request, res: Response) {
    const list = await this.application.list();
    const result = new UserListMapping().execute(
      list.map(user => user.properties()),
    );
    return res.json(result);
  }

  async listOne(req: Request, res: Response) {
    const { guid } = req.params;
    const guidResult = GuidVo.create(guid);
    if (guidResult.isErr()) {
      const err: IError = new Error(guidResult.error.message);
      return res.status(400).json({ message: err.message });
    }

    // se inyecta al pasar la verificación de JWT
    if (guid !== req.body.user.guid) {
      const err: IError = new Error('No tienes permisos para ver este usuario');
      return res.status(401).json({ message: err.message });
    }

    const userResult = await this.application.listOne(guid);
    if (userResult.isErr()) {
      const err: IError = new Error(userResult.error.message);
      return res.status(404).json({ message: err.message });
    }

    if (userResult.isOk()) {
      const result = new UserListOneMapping().execute(
        userResult.value.properties(),
      );
      return res.json(result);
    }
  }

  async update(req: Request, res: Response) {
    const { guid } = req.params;
    const { name, lastname, password } = req.body;

    const guidResult = GuidVo.create(guid);
    if (guidResult.isErr()) {
      const err: IError = new Error(guidResult.error.message);
      return res.status(400).json({ message: err.message });
    }

    // se inyecta al pasar la verificación de JWT
    if (guid !== req.body.user.guid) {
      const err: IError = new Error(
        'No tienes permisos para actualizar este usuario',
      );
      return res.status(401).json({ message: err.message });
    }

    const dataResult = await this.application.update(guid, {
      name,
      lastname,
      password,
    });
    if (dataResult.isErr()) {
      const err: IError = new Error(dataResult.error.message);
      return res.status(404).json({ message: err.message });
    }
    if (dataResult.isOk()) {
      const result = new UserUpdateMapping().execute(
        dataResult.value.properties(),
      );
      return res.json(result);
    }
  }

  async delete(req: Request, res: Response) {
    const { guid } = req.params;
    const guidResult = GuidVo.create(guid);
    if (guidResult.isErr()) {
      const err: IError = new Error(guidResult.error.message);
      return res.status(400).json({ message: err.message });
    }

    // se inyecta al pasar la verificación de JWT
    if (guid !== req.body.user.guid) {
      const err: IError = new Error(
        'No tienes permisos para eliminar este usuario',
      );
      return res.status(401).json({ message: err.message });
    }

    const dataResult = await this.application.delete(guid);
    if (dataResult.isErr()) {
      const err: IError = new Error(dataResult.error.message);
      return res.status(404).json({ message: err.message });
    }
    if (dataResult.isOk()) {
      const result = new UserDeleteMapping().execute(
        dataResult.value.properties(),
      );
      return res.json(result);
    }
  }
}
