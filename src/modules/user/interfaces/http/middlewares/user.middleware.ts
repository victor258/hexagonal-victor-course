import { validate } from 'class-validator';
import { NextFunction, Request, Response } from 'express';
import { UserParamsGuidValidator } from '../validators/UserparamsGuid.validator';
import { BodyUpdate } from '../validators/UserBodyUpdate.validator';
import { BodyInsert } from '../validators/UserBodyInsert.validator';
import { BodyLogin } from '../validators/UserBodyLogin.validator';
import { VerifyTokenMiddleware } from '../../../../shared/verifyToken.middleware';

class UserMiddleware {
  static async ValidateParamsGuid(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const { guid } = req.params;
    const userParamsGuidValidator = new UserParamsGuidValidator();
    userParamsGuidValidator.guid = guid;
    const errors = await validate(userParamsGuidValidator);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }

  static async ValidateBodyUpdate(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const body = new BodyUpdate();
    body.name = req.body.name;
    body.lastname = req.body.lastname;
    body.password = req.body.password;
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }

  static async ValidateBodyInsert(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const body = new BodyInsert();
    body.name = req.body.name;
    body.lastname = req.body.lastname;
    body.email = req.body.email;
    body.password = req.body.password;
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }

  static async ValidateBodyLogin(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const body = new BodyLogin();
    body.email = req.body.email;
    body.password = req.body.password;
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }
}

export const MiddlewareParamsGuidValidator = UserMiddleware.ValidateParamsGuid;
export const MiddlewareUpdateValidator = UserMiddleware.ValidateBodyUpdate;
export const MiddlewareInsertValidator = UserMiddleware.ValidateBodyInsert;
export const MiddlewareLoginValidator = UserMiddleware.ValidateBodyLogin;
export const MiddlewareVerifyToken = VerifyTokenMiddleware.execute;
