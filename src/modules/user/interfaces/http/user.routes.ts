import { Router } from 'express';

import UserApplication from '../../application/user.application';
import UserInfraestructure from '../../infraestructure/user.infraestructure';
import userController from './user.controller';

import { UserRepository } from '../../domain/user.repository';
import {
  MiddlewareParamsGuidValidator,
  MiddlewareUpdateValidator,
  MiddlewareInsertValidator,
  MiddlewareLoginValidator,
  MiddlewareVerifyToken,
} from './middlewares/user.middleware';

const infraestructure: UserRepository = new UserInfraestructure();
const application: UserApplication = new UserApplication(infraestructure);
const controller = new userController(application);

class UserRouter {
  readonly router: Router;

  constructor() {
    this.router = Router();
    this.mountRoutes();
  }

  mountRoutes() {
    // design pattern: chain of responsability
    this.router.post('/register', MiddlewareInsertValidator, controller.insert);
    this.router.post('/login', MiddlewareLoginValidator, controller.login);
    this.router.get('/', MiddlewareVerifyToken, controller.list);
    this.router.get(
      '/:guid',
      MiddlewareParamsGuidValidator,
      MiddlewareVerifyToken,
      controller.listOne,
    );
    this.router.put(
      '/:guid',
      MiddlewareParamsGuidValidator,
      MiddlewareUpdateValidator,
      MiddlewareVerifyToken,
      controller.update,
    );
    this.router.delete(
      '/:guid',
      MiddlewareParamsGuidValidator,
      MiddlewareVerifyToken,
      controller.delete,
    );
  }
}

export default new UserRouter().router;
