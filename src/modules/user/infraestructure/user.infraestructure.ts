import { err, ok, Result } from 'neverthrow';

import DatabaseBootstrap from '../../../bootstrap/database.bootstrap';
import {
  UserEmailAlreadyExistsException,
  UserEmailInvalidException,
  UserNotFoundException,
  UserPasswordInvalidException,
} from '../domain/exceptions/user.exception';
import User from '../domain/user';
import { UserUpdate } from '../domain/interfaces/userUpdate.interface';
import { UserRepository } from '../domain/user.repository';
import { EmailVO } from '../domain/value-objects/email.vo';
import { UserEntity } from './user.entity';
import { UserPasswordService } from '../domain/services/user-password.service';

export default class UserInfraestructure implements UserRepository {
  async login(
    user: User,
  ): Promise<Result<User, UserNotFoundException | UserEmailInvalidException>> {
    const { email, password, token } = user.properties();

    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);

    const userDB = await repo.findOne({
      where: { email: email.value, active: true },
    });

    if (!userDB) {
      return err(new UserNotFoundException());
    }

    const passwordResult = await UserPasswordService.compare(
      password,
      userDB.password,
    );

    if (!passwordResult) {
      return err(new UserPasswordInvalidException());
    }

    // 🤙🏽 Actualizar JWT en DB
    const userEntity = await repo.save({ ...userDB, token });

    return ok(
      new User({
        guid: userEntity.guid,
        name: userEntity.name,
        lastname: userEntity.lastname,
        password: userEntity.password,
        email,
        token: userEntity.token,
      }),
    );
  }

  async insert(
    user: User,
  ): Promise<Result<User, UserNotFoundException | UserEmailInvalidException>> {
    const { guid, name, lastname, email, password, token, active } =
      user.properties();

    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);
    const userDB = await repo.findOne({
      where: { email: email.value, active: true },
    });

    if (userDB) {
      return err(new UserEmailAlreadyExistsException());
    }

    const userInsert = new UserEntity();
    Object.assign(userInsert, {
      guid,
      name,
      lastname,
      email: email.value,
      password,
      token,
      active,
    });

    await repo.save(userInsert);

    return ok(user);
  }

  async list(): Promise<User[]> {
    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);
    const result = await repo.find({ where: { active: true } });
    return result.map((user: UserEntity) => {
      const emailResult = EmailVO.create(user.email);

      if (emailResult.isErr()) {
        throw new UserEmailInvalidException();
      }

      const { guid, name, lastname, password, token, active } = user;

      return new User({
        guid,
        name,
        lastname,
        email: emailResult.value,
        password,
        token,
        active,
      });
    });
  }

  async listOne(
    guid: string,
  ): Promise<Result<User, UserNotFoundException | UserEmailInvalidException>> {
    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);
    const result = await repo.findOne({ where: { guid } });

    if (!result) {
      return err(new UserNotFoundException());
    }

    const emailResult = EmailVO.create(result.email);
    if (emailResult.isErr()) {
      return err(new UserEmailInvalidException());
    }

    const { name, lastname, password, token, active } = result;

    return ok(
      new User({
        guid,
        name,
        lastname,
        email: emailResult.value,
        password,
        token,
        active,
      }),
    );
  }

  async update(
    guid: string,
    user: Partial<UserUpdate>,
  ): Promise<Result<User, UserNotFoundException | UserEmailInvalidException>> {
    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);

    const userFound = await repo.findOne({
      where: { guid },
    });

    if (!userFound) {
      return err(new UserNotFoundException());
    }

    Object.assign(userFound, {
      ...user,
      password:
        user.password && (await UserPasswordService.hash(user.password)),
    });

    const userEntity = await repo.save(userFound);
    const emailResult = EmailVO.create(userEntity.email);

    if (emailResult.isErr()) {
      return err(new UserEmailInvalidException());
    }

    const { name, lastname, password, token, active } = userEntity;

    return ok(
      new User({
        guid,
        name,
        lastname,
        email: emailResult.value,
        password,
        token,
        active,
      }),
    );
  }

  async delete(
    guid: string,
  ): Promise<Result<User, UserNotFoundException | UserEmailInvalidException>> {
    const repo = DatabaseBootstrap.dataSource.getRepository(UserEntity);

    const userFound = await repo.findOne({
      where: { guid },
    });

    if (!userFound) {
      return err(new UserNotFoundException());
    }

    userFound.active = false;
    const userEntity = await repo.save(userFound);
    const emailResult = EmailVO.create(userEntity.email);

    if (emailResult.isErr()) {
      return err(new UserEmailInvalidException());
    }

    const { name, lastname, password, token, active } = userEntity;

    return ok(
      new User({
        guid,
        name,
        lastname,
        email: emailResult.value,
        password,
        token,
        active,
      }),
    );
  }
}
