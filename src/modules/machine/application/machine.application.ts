import Machine from '../domain/machine';
import { MachineUpdate } from '../domain/interfaces/machineUpdate.interface';
import { MachineRepository } from '../domain/machine.repository';

export default class MachineApplication {
  // SOLID PRINCIPLE: Inversion Dependency
  // DESING PATTERN: Dependency Injection
  constructor(private readonly machineRepository: MachineRepository) {}

  insert(machine: Machine) {
    return this.machineRepository.insert(machine);
  }

  list() {
    return this.machineRepository.list();
  }

  listOne(guid: string) {
    return this.machineRepository.listOne(guid);
  }

  update(guid: string, machine: Partial<MachineUpdate>) {
    return this.machineRepository.update(guid, machine);
  }

  delete(
    guid: string,
    {
      emailUser,
    }: {
      emailUser: string;
    },
  ) {
    return this.machineRepository.delete(guid, { emailUser });
  }
}
