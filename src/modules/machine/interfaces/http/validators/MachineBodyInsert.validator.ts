import {
  IsNotEmpty,
  IsString,
  MinLength,
  IsInt,
  Min,
  Matches,
} from 'class-validator';

export class BodyInsert {
  @IsString({ message: 'El nombre de la máquina debe ser un string' })
  @IsNotEmpty({ message: 'El nombre de la máquina no puede estar vacío' })
  @MinLength(2, {
    message: 'El nombre de la máquina debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El nombre de la máquina no puede contener solo espacios vacíos',
  })
  name: string;

  @IsInt({ message: 'El precio de la máquina debe ser un número entero' })
  @IsNotEmpty({ message: 'El precio de la máquina no puede estar vacío' })
  @Min(0, { message: 'El precio de la máquina debe ser mayor a 0' })
  price: number;

  @IsString({ message: 'La patente de la máquina debe ser un string' })
  @IsNotEmpty({ message: 'La patente de la máquina no puede estar vacía' })
  @MinLength(2, {
    message: 'La patente de la máquina debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'La patente de la máquina no puede contener solo espacios vacíos',
  })
  patent: string;
}
