import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class MachineParamsIdValidator {
  @IsString({ message: 'El id de la máquina debe ser un string' })
  @IsNotEmpty({ message: 'El id de la máquina no puede estar vacío' })
  @MinLength(10, {
    message: 'El id de la máquina debe tener al menos 10 caracteres',
  })
  machine_id: string;
}
