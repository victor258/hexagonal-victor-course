import {
  IsNotEmpty,
  IsString,
  MinLength,
  IsInt,
  Min,
  IsBoolean,
  Matches,
  IsOptional,
} from 'class-validator';

export class BodyUpdate {
  @IsOptional()
  @IsString({ message: 'El nombre de la máquina debe ser un string' })
  @IsNotEmpty({ message: 'El nombre de la máquina no puede estar vacío' })
  @MinLength(2, {
    message: 'El nombre de la máquina debe tener al menos 2 caracteres',
  })
  @Matches(/^(?!^\s*$).+/, {
    message: 'El nombre de la máquina no puede contener solo espacios vacíos',
  })
  name?: string;

  @IsOptional()
  @IsInt({ message: 'El precio de la máquina debe ser un número entero' })
  @IsNotEmpty({ message: 'El precio de la máquina no puede estar vacío' })
  @Min(0, { message: 'El precio de la máquina debe ser mayor a 0' })
  price?: number;

  @IsOptional()
  @IsBoolean({ message: 'El estado de la máquina debe ser un booleano' })
  @IsNotEmpty({ message: 'El estado de la máquina no puede estar vacío' })
  available?: boolean;
}
