import { Request, Response } from 'express';
import { IError } from '../helpers/ierror';
import { Machine_idVo } from '../../domain/value-objects/machine_id.vo';

import MachineFactory from '../../domain/machine-factory';
import MachineApplication from '../../application/machine.application';

import { MachineDeleteMapping } from './dto/machine-delete.dto';
import { MachineInsertMapping } from './dto/machine-insert.dto';
import { MachineListOneMapping } from './dto/machine-list-one.dto';
import { MachineListMapping } from './dto/machine-list.dto';
import { MachineUpdateMapping } from './dto/machine-update.dto';

export default class {
  // Design Pattern: Dependency Injection
  // Solid Principle: Dependency Inversion
  constructor(private application: MachineApplication) {
    // Design Pattern: mediator or controller
    this.insert = this.insert.bind(this);
    this.list = this.list.bind(this);
    this.listOne = this.listOne.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  async insert(req: Request, res: Response) {
    const { name, patent, price } = req.body;
    const machineResult = new MachineFactory().create({
      name,
      patent,
      price,
      email: req.body.user.email,
    });

    if (machineResult.isErr()) {
      const err: IError = new Error(machineResult.error.message);
      return res.status(400).json({ message: err.message });
    }

    const data = await this.application.insert(machineResult.value);
    const result = new MachineInsertMapping().execute(data.properties());
    return res.status(201).json(result);
  }

  async list(_req: Request, res: Response) {
    const data = await this.application.list();
    const result = new MachineListMapping().execute(
      data.map(machine => machine.properties()),
    );
    return res.json(result);
  }

  async listOne(req: Request, res: Response) {
    const { machine_id } = req.params;
    const machine_idVo = Machine_idVo.create(machine_id);

    if (machine_idVo.isErr()) {
      const err: IError = new Error(machine_idVo.error.message);
      return res.status(400).json({ message: err.message });
    }

    const machineResult = await this.application.listOne(machine_id);
    if (machineResult.isErr()) {
      const err: IError = new Error(machineResult.error.message);
      return res.status(404).json({ message: err.message });
    }

    const result = new MachineListOneMapping().execute(
      machineResult.value.properties(),
    );
    return res.json(result);
  }

  async update(req: Request, res: Response) {
    const { machine_id } = req.params;
    const { name, available, price } = req.body;

    const machine_idVo = Machine_idVo.create(machine_id);
    if (machine_idVo.isErr()) {
      const err: IError = new Error(machine_idVo.error.message);
      return res.status(400).json({ message: err.message });
    }

    const machineResult = await this.application.update(machine_id, {
      name,
      available,
      price,
      email: req.body.user.email,
    });
    if (machineResult.isErr()) {
      const err: IError = new Error(machineResult.error.message);
      return res.status(404).json({ message: err.message });
    }

    const result = new MachineUpdateMapping().execute(
      machineResult.value.properties(),
    );
    return res.json(result);
  }

  async delete(req: Request, res: Response) {
    const { machine_id } = req.params;
    const machine_idVo = Machine_idVo.create(machine_id);

    if (machine_idVo.isErr()) {
      const err: IError = new Error(machine_idVo.error.message);
      return res.status(400).json({ message: err.message });
    }

    const machineResult = await this.application.delete(machine_id, {
      emailUser: req.body.user.email,
    });
    if (machineResult.isErr()) {
      const err: IError = new Error(machineResult.error.message);
      return res.status(404).json({ message: err.message });
    }

    const result = new MachineDeleteMapping().execute(
      machineResult.value.properties(),
    );
    return res.json(result);
  }
}
