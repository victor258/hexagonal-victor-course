import { Router } from 'express';

import MachineApplication from '../../application/machine.application';
import MachineController from './machine.controllers';
import MachineInfraestructure from '../../infraestructure/machine.infraestructure';

import { MachineRepository } from '../../domain/machine.repository';
import {
  MiddlewareParamsIdValidator,
  MiddlewareUpdateValidator,
  MiddlewareInsertValidator,
  MiddlewareVerifyToken,
} from './middlewares/machine.middleware';

const infraestructure: MachineRepository = new MachineInfraestructure();
const application: MachineApplication = new MachineApplication(infraestructure);
const controller = new MachineController(application);

class MachineRouter {
  readonly router: Router;

  constructor() {
    this.router = Router();
    this.mountRoutes();
  }

  mountRoutes() {
    // design pattern: chain of responsability
    this.router.post(
      '/',
      MiddlewareInsertValidator,
      MiddlewareVerifyToken,
      controller.insert,
    );
    this.router.get('/', controller.list);
    this.router.get(
      '/:machine_id',
      MiddlewareParamsIdValidator,
      MiddlewareVerifyToken,
      controller.listOne,
    );
    this.router.put(
      '/:machine_id',
      MiddlewareParamsIdValidator,
      MiddlewareUpdateValidator,
      MiddlewareVerifyToken,
      controller.update,
    );
    this.router.delete(
      '/:machine_id',
      MiddlewareParamsIdValidator,
      MiddlewareVerifyToken,
      controller.delete,
    );
  }
}

export default new MachineRouter().router;
