import { validate } from 'class-validator';
import { NextFunction, Request, Response } from 'express';
import { MachineParamsIdValidator } from '../validators/MachineParamsId.validator';
import { BodyUpdate } from '../validators/MachineBodyUpdate.validator';
import { BodyInsert } from '../validators/MachineBodyInsert.validator';
import { VerifyTokenMiddleware } from '../../../../shared/verifyToken.middleware';

class MachineMiddleware {
  static async validateMachineParamsId(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const machineParamsIdValidator = new MachineParamsIdValidator();
    machineParamsIdValidator.machine_id = req.params.machine_id;
    const errors = await validate(machineParamsIdValidator);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }
  static async validateMachineNameValidator(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const body = new BodyUpdate();
    body.name = req.body.name;
    body.price = req.body.price;
    body.available = req.body.available;
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }

  static async validateMachineBodyInsert(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const body = new BodyInsert();
    body.name = req.body.name;
    body.price = req.body.price;
    body.patent = req.body.patent;
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    next();
  }
}

// TS infiere: const MiddlewareParamsId: ((req: Request, res: Response, next: NextFunction) => Promise<void>)[]
export const MiddlewareParamsIdValidator =
  MachineMiddleware.validateMachineParamsId;
export const MiddlewareUpdateValidator =
  MachineMiddleware.validateMachineNameValidator;
export const MiddlewareInsertValidator =
  MachineMiddleware.validateMachineBodyInsert;

export const MiddlewareVerifyToken = VerifyTokenMiddleware.execute;
