import { MachineProperties } from '../../../domain/types/machineProperties.type';
import { DTO } from './dto.generic';

export interface MachineListOneDTO {
  machine_id: string;
  name: string;
  price: number;
  patent: string;
  available: boolean;
  email: string;
}

export class MachineListOneMapping extends DTO<
  MachineProperties,
  MachineListOneDTO
> {
  execute(data: MachineProperties): MachineListOneDTO {
    return {
      machine_id: data.machine_id,
      name: data.name,
      price: data.price,
      patent: data.patent,
      available: data.available,
      email: data.email,
    };
  }
}
