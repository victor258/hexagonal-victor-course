import { MachineProperties } from '../../../domain/types/machineProperties.type';
import { DTO } from './dto.generic';

export interface MachineInsertDTO {
  machine_id: string;
  name: string;
  price: number;
  patent: string;
  available: boolean;
  email: string;
}

export class MachineInsertMapping extends DTO<
  MachineProperties,
  MachineInsertDTO
> {
  execute(data: MachineProperties): MachineInsertDTO {
    return {
      machine_id: data.machine_id,
      name: data.name,
      price: data.price,
      patent: data.patent,
      available: data.available,
      email: data.email,
    };
  }
}
