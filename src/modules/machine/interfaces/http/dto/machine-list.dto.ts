import { MachineProperties } from '../../../domain/types/machineProperties.type';
import { DTO } from './dto.generic';

export interface MachineListDTO {
  machine_id: string;
  name: string;
  price: number;
  patent: string;
  available: boolean;
  email: string;
}

export class MachineListMapping extends DTO<
  MachineProperties[],
  MachineListDTO[]
> {
  execute(data: MachineProperties[]): MachineListDTO[] {
    return data.map(machine => ({
      machine_id: machine.machine_id,
      name: machine.name,
      price: machine.price,
      patent: machine.patent,
      available: machine.available,
      email: machine.email,
    }));
  }
}
