import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class MachineEntity {
  @PrimaryColumn()
  machine_id: string;

  @Column({ type: 'varchar', length: 20, nullable: false, unique: true })
  patent: string;

  @Column({ type: 'varchar', length: 100 })
  name: string;

  @Column({ type: 'int', default: 0 })
  price: number;

  // Por si la máquina está en mantenimiento
  @Column({ type: 'boolean', default: true })
  available: boolean;

  // Para eliminar de forma lógica la machine
  @Column({ type: 'boolean', default: true })
  active: boolean;

  @Column({ type: 'varchar', length: 200 })
  email: string;
}
