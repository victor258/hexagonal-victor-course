import DatabaseBootstrap from '../../../bootstrap/database.bootstrap';
import {
  MachineEmailInconsistentException,
  MachineNotFoundException,
} from '../domain/exceptions/machine.exception';
import Machine from '../domain/machine';
import { MachineUpdate } from '../domain/interfaces/machineUpdate.interface';
import { MachineRepository } from '../domain/machine.repository';
import { MachineEntity } from './machine.entity';
import { err, ok, Result } from 'neverthrow';

export default class MachineInfraestructure implements MachineRepository {
  async insert(machine: Machine): Promise<Machine> {
    const machineInsert = new MachineEntity();
    const { machine_id, name, price, patent, available, active, email } =
      machine.properties();
    Object.assign(machineInsert, {
      machine_id,
      name,
      price,
      patent,
      available,
      active,
      email,
    });
    await DatabaseBootstrap.dataSource
      .getRepository(MachineEntity)
      .save(machineInsert);
    return machine;
  }

  async list(): Promise<Machine[]> {
    const repo = DatabaseBootstrap.dataSource.getRepository(MachineEntity);
    const result = await repo.find({ where: { active: true } });
    return result.map(machine => {
      const { machine_id, name, price, patent, available, active, email } =
        machine;
      return new Machine({
        machine_id,
        name,
        price,
        patent,
        available,
        active,
        email,
      });
    });
  }

  async listOne(
    machine_id: string,
  ): Promise<Result<Machine, MachineNotFoundException>> {
    const repo = DatabaseBootstrap.dataSource.getRepository(MachineEntity);
    const result = await repo.findOne({ where: { machine_id } });
    if (!result) {
      return err(new MachineNotFoundException());
    }
    const { name, price, patent, available, active, email } = result;
    return ok(
      new Machine({
        machine_id,
        name,
        price,
        patent,
        available,
        active,
        email,
      }),
    );
  }

  async update(
    machine_id: string,
    machineUpdate: MachineUpdate,
  ): Promise<
    Result<
      Machine,
      MachineNotFoundException | MachineEmailInconsistentException
    >
  > {
    const repo = DatabaseBootstrap.dataSource.getRepository(MachineEntity);
    const result = await repo.findOne({ where: { machine_id } });
    if (!result) {
      return err(new MachineNotFoundException());
    }

    if (result.email !== machineUpdate.email) {
      return err(new MachineEmailInconsistentException());
    }

    Object.assign(result, machineUpdate);
    const machineEntity = await repo.save(result);
    const { name, price, patent, available, active, email } = machineEntity;

    return ok(
      new Machine({
        machine_id,
        name,
        price,
        patent,
        available,
        active,
        email,
      }),
    );
  }

  async delete(
    machine_id: string,
    {
      emailUser,
    }: {
      emailUser: string;
    },
  ): Promise<
    Result<
      Machine,
      MachineNotFoundException | MachineEmailInconsistentException
    >
  > {
    const repo = DatabaseBootstrap.dataSource.getRepository(MachineEntity);
    const result = await repo.findOne({ where: { machine_id } });
    if (!result) {
      return err(new MachineNotFoundException());
    }

    if (result.email !== emailUser) {
      return err(new MachineEmailInconsistentException());
    }

    result.active = false;
    const machineEntity = await repo.save(result);

    const { name, price, patent, available, active, email } = machineEntity;

    return ok(
      new Machine({
        machine_id,
        name,
        price,
        patent,
        available,
        active,
        email,
      }),
    );
  }
}
