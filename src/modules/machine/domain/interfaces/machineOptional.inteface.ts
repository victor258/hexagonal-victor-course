export interface MachineOptional {
  machine_id: string;
  available: boolean;
  active: boolean;
}
