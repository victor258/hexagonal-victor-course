export interface MachineRequired {
  name: string;
  patent: string;
  price: number;
  email: string;
}
