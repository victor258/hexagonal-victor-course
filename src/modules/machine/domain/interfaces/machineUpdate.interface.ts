export interface MachineUpdate {
  name?: string;
  available?: boolean;
  price?: number;
  email: string;
}
