import { IEntity } from '../../shared/entity.interface';
import { MachineUpdate } from './interfaces/machineUpdate.interface';
import { MachineProperties } from './types/machineProperties.type';

export default class Machine
  implements IEntity<MachineProperties, MachineUpdate>
{
  private readonly machine_id: string;
  private name: string;
  private patent: string;
  private price: number;
  private available: boolean;
  private active: boolean;
  private email: string;

  constructor(properties: MachineProperties) {
    this.active = true;
    this.available = true;
    Object.assign(this, properties);
  }

  properties(): MachineProperties {
    return {
      name: this.name,
      machine_id: this.machine_id,
      patent: this.patent,
      price: this.price,
      available: this.available,
      active: this.active,
      email: this.email,
    };
  }

  update(fields: MachineUpdate) {
    Object.assign(this, fields);
  }

  delete() {
    this.active = false;
  }
}
