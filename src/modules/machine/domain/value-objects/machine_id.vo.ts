import { validate as uuidValidate } from 'uuid';
import { ValueObject } from './vo.class';
import { MachineIdInvalidException } from '../exceptions/machine.exception';
import { err, ok, Result } from 'neverthrow';

interface IdProps {
  value: string;
}

type IdResult = Result<Machine_idVo, MachineIdInvalidException>;

export class Machine_idVo extends ValueObject<IdProps> {
  private constructor(props: IdProps) {
    super(props);
  }

  static create(id: string): IdResult {
    if (!uuidValidate(id)) {
      return err(new MachineIdInvalidException());
    }

    return ok(new Machine_idVo({ value: id }));
  }

  get value(): string {
    return this.props.value;
  }
}
