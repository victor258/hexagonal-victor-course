import { MachineOptional } from '../interfaces/machineOptional.inteface';
import { MachineRequired } from '../interfaces/machineRequired.interface';

export type MachineProperties = Required<MachineRequired> &
  Partial<MachineOptional>;
