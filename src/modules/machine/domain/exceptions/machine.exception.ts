import { DomainException } from './domain.exception';
import { DomainExceptionCode } from '../enums/domainException.enum';

export class MachineNameRequiredException extends DomainException {
  constructor() {
    super(MachineNameRequiredException.getMessage());
    this.name = DomainExceptionCode.MACHINE_NAME_REQUIRED;
  }

  static getMessage() {
    return 'Name is required';
  }
}

export class MachinePriceRequiredException extends DomainException {
  constructor() {
    super(MachinePriceRequiredException.getMessage());
    this.name = DomainExceptionCode.MACHINE_PRICE_REQUIRED;
  }

  static getMessage() {
    return 'Price is required';
  }
}

export class MachineIdRequiredException extends DomainException {
  constructor() {
    super(MachineIdRequiredException.getMessage());
    this.name = DomainExceptionCode.MACHINE_ID_REQUIRED;
  }

  static getMessage() {
    return 'Id is required';
  }
}

export class MachineIdInvalidException extends DomainException {
  constructor() {
    super(MachineIdInvalidException.getMessage());
    this.name = DomainExceptionCode.MACHINE_ID_INVALID;
  }

  static getMessage() {
    return 'Id is invalid';
  }
}

export class MachinePatentRequiredException extends DomainException {
  constructor() {
    super(MachinePatentRequiredException.getMessage());
    this.name = DomainExceptionCode.MACHINE_PATENT_REQUIRED;
  }

  static getMessage() {
    return 'Patent is required';
  }
}

export class MachineNotFoundException extends DomainException {
  constructor() {
    super(MachineNotFoundException.getMessage());
    this.name = DomainExceptionCode.MACHINE_NOT_FOUND;
  }

  static getMessage() {
    return 'Machine not found';
  }
}

export class MachineEmailInconsistentException extends DomainException {
  constructor() {
    super(MachineEmailInconsistentException.getMessage());
    this.name = DomainExceptionCode.MACHINE_EMAIL_INCONSISTENT;
  }

  static getMessage() {
    return 'Email no coincide con user';
  }
}
