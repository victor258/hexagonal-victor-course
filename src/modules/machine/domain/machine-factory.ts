import { v4 as uuidv4 } from 'uuid';
import { MachineProperties } from './types/machineProperties.type';
import { err, ok, Result } from 'neverthrow';
import Machine from './machine';

import {
  MachineNameRequiredException,
  MachinePriceRequiredException,
  MachinePatentRequiredException,
} from './exceptions/machine.exception';

interface MachineCreateProps {
  name: string;
  price: number;
  patent: string;
  email: string;
}

// design pattern AbstractFactory
export default class MachineFactory {
  create({
    name,
    price,
    patent,
    email,
  }: MachineCreateProps): Result<
    Machine,
    | MachineNameRequiredException
    | MachinePriceRequiredException
    | MachinePatentRequiredException
  > {
    if (!name || !name.trim()) {
      return err(new MachineNameRequiredException());
    }

    if (!price || price < 0) {
      return err(new MachinePriceRequiredException());
    }

    if (!patent || !patent.trim()) {
      return err(new MachinePatentRequiredException());
    }

    const machineProperties: MachineProperties = {
      name,
      price,
      patent,
      machine_id: uuidv4(),
      email,
    };

    const machine = new Machine(machineProperties);

    return ok(machine);
  }
}
