import { Result } from 'neverthrow';
import Machine from './machine';
import { MachineUpdate } from './interfaces/machineUpdate.interface';
import { MachineNotFoundException } from './exceptions/machine.exception';

// Princio SOLID: Dependency Inversion Principle
export interface MachineRepository {
  // design pattern Facade
  list(): Promise<Machine[]>;
  listOne(
    machine_id: string,
  ): Promise<Result<Machine, MachineNotFoundException>>;
  insert(machine: Machine): Promise<Machine>;
  update(
    machine_id: string,
    machine: Partial<MachineUpdate>,
  ): Promise<Result<Machine, MachineNotFoundException>>;
  delete(
    machine_id: string,
    {
      emailUser,
    }: {
      emailUser: string;
    },
  ): Promise<Result<Machine, MachineNotFoundException>>;
}
