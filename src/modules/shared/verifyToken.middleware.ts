import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import databaseBootstrap from '../../bootstrap/database.bootstrap';
import { UserEntity } from '../user/infraestructure/user.entity';

declare module 'jsonwebtoken' {
  export interface UserIDJwtPayload extends jwt.JwtPayload {
    email: string;
  }
}

export class VerifyTokenMiddleware {
  static async execute(
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    const { authorization } = req.headers;

    if (!authorization) {
      res.status(401).json({ error: 'Token not found' });
      return;
    }

    // Bearer token
    const token = authorization.split(' ')[1];

    try {
      const { email } = <jwt.UserIDJwtPayload>(
        jwt.verify(token, process.env.JWT_SECRET || 'secret')
      );

      const userDB = await databaseBootstrap.dataSource
        .getRepository(UserEntity)
        .findOne({ where: { email } });

      if (userDB.token !== token) {
        res.status(401).json({ error: 'Token incorrect' });
        return;
      }

      req.body.user = userDB;

      next();
    } catch (error) {
      res.status(401).json({ error: 'Token not found' });
      return;
    }
  }
}
