export interface IEntity<Properties, PropertiesUpate> {
  properties: () => Properties;
  delete: () => void;
  update: (properties: PropertiesUpate) => void;
}
