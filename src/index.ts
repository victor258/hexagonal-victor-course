import { Bootstrap } from './bootstrap/base.bootstrap';
import ServerBootstrap from './bootstrap/server.bootstrap';
import DatabaseBootstrap from './bootstrap/database.bootstrap';
import app from './app';

const serverBootstrap: Bootstrap = new ServerBootstrap(app);
const databaseBootstrap: Bootstrap = new DatabaseBootstrap();

(async () => {
  try {
    await databaseBootstrap.initialize();
    await serverBootstrap.initialize();
    console.log('Server initialized');
  } catch (error) {
    console.log(error);
  }
})();
