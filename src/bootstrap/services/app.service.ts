import { DB_CONFIG } from '../interfaces/dbConfig.interface';

export class AppService {
  static get PORT(): number {
    return +process.env.PORT || 3000;
  }

  static get DBConfig(): DB_CONFIG {
    return {
      host: process.env.DB_HOST || 'localhost',
      port: +process.env.DB_PORT || 3308,
      // entities: [process.env.DB_ENTITIES || 'src/**/*.entity.{ts,js}'],
      entities: [process.env.DB_ENTITIES || 'dist/**/*.entity.{ts,js}'],
      username: process.env.DB_USERNAME || 'admin',
      password: process.env.DB_PASSWORD || 'root',
      database: process.env.DB_NAME || 'bddcursonode',
      synchronize: process.env.DB_SYNC === 'true' || true,
      logging: process.env.DB_LOG === 'true' ? true : false,
    };
  }
}
