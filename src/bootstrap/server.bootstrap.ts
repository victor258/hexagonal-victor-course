import http from 'http';
import { Application } from 'express';
import { Bootstrap } from './base.bootstrap';
import { AppService } from './services/app.service';

class ServerBootstrap extends Bootstrap {
  constructor(private readonly app: Application) {
    super();
  }

  // principio SOLID: Liskov Substitution Principle
  // principio SOLID: Single Responsibility Principle
  initialize(): Promise<string | Error> {
    return new Promise((_resolve, reject) => {
      const server = http.createServer(this.app);

      server
        .listen(AppService.PORT)
        .on('listening', () => {
          // resolve('Server is running on port 3000');
          console.log('Server is running on port: ' + AppService.PORT);
        })
        .on('error', (err: Error) => {
          reject(err);
          console.log(`Server error on port: ${AppService.PORT}`);
        });
    });
  }
}

export default ServerBootstrap;
