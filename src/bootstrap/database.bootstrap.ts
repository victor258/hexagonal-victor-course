import { DataSource } from 'typeorm';
import { Bootstrap } from './base.bootstrap';
// import { UserEntity } from '../modules/user/infraestructure/user.entity';
import { AppService } from './services/app.service';

let appDataSource: DataSource;

export default class extends Bootstrap {
  initialize(): Promise<DataSource> {
    const AppDataSource = new DataSource({
      type: 'mysql',
      ...AppService.DBConfig,
    });

    appDataSource = AppDataSource;

    return AppDataSource.initialize();
  }

  static get dataSource(): DataSource {
    return appDataSource;
  }
}
